export default class CustomCanvas {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  constructor(sketch: Function, container: HTMLElement) {
    const self = this;
    this.canvas = document.createElement("canvas") as HTMLCanvasElement;
    this.ctx = self.canvas.getContext("2d") as CanvasRenderingContext2D;

    self.ctx.fillStyle = "black";
    self.canvas.setAttribute("height", "80");

    function loop() {
      self.canvas.width = Math.round(
        window.devicePixelRatio * self.canvas.clientWidth
      );
      self.canvas.height = Math.round(
        window.devicePixelRatio * self.canvas.clientHeight
      );

      //   // console.log(canvas.width, canvas.height)
      //   // console.log(canvas.clientWidth, canvas.clientHeight)
      //   // console.log(window.devicePixelRatio)
      //   ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
      self.ctx.translate(0.5, -0.5);
      sketch(self.ctx, self);
      window.requestAnimationFrame(loop);
    }
    window.requestAnimationFrame(loop);
    container.appendChild(self.canvas);
  }

  line(x1: number, y1: number, x2: number, y2: number) {
    this.ctx.beginPath();
    this.ctx.moveTo(Math.round(x1), Math.round(y1));
    this.ctx.lineTo(Math.round(x2), Math.round(y2));
    this.ctx.stroke();
  }

  stroke(r: number, g?: number, b?: number, a: number = 255) {
    if (g == null) {
      g = r;
    }
    if (b == null) {
      b = r;
    }
    this.ctx.strokeStyle = "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
  }
  
  background(r: number, g?: number, b?: number, a: number = 255) {
    if (g == null) {
      g = r;
    }
    if (b == null) {
      b = r;
    }
    this.ctx.fillStyle = "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
  }
}
