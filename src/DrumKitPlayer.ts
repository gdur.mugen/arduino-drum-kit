import { Howl, Howler } from "howler";

export enum DrumTypes {
  Crash = "Crash",
  Ride = "Ride",
  FloorTom = "Floor Tom",
  MidTom = "Mid Tom",
  HighTom = "High Tom",
  Kick = "Kick",
  Snare = "Snare",
  HiHat = "Hi-Hat",
  HiHatOpen = "Hi-Hat Open",
  HiHatClosed = "Hi-Hat Closed",
}

export class DrumKitPlayer {
  sounds: Howl[];
  public static types = DrumTypes;

  constructor() {
    this.sounds = [
      new Howl({
        src: ["../src/assets/sounds/crash.wav"],
        volume: 0.3,
      }),
      new Howl({
        src: ["../src/assets/sounds/snare.wav"],
        volume: 1,
      }),
      new Howl({
        src: ["../src/assets/sounds/kick.wav"],
        volume: 1,
      }),
      new Howl({
        src: ["../src/assets/sounds/hihat-close.wav"],
        volume: 1,
      }),
      new Howl({
        src: ["../src/assets/sounds/hihat-open.wav"],
        volume: 1,
      }),
      new Howl({
        src: ["../src/assets/sounds/ride.wav"],
        volume: 1,
      }),
      new Howl({
        src: ["../src/assets/sounds/tom-high.wav"],
        volume: 1,
      }),
      new Howl({
        src: ["../src/assets/sounds/tom-mid.wav"],
        volume: 1,
      }),
      new Howl({
        src: ["../src/assets/sounds/tom-low.wav"],
        volume: 1,
      }),
    ];
  }

  setVolume(volume: number) {
    if (volume > 1) volume = 1;
    if (volume < 0) volume = 0;

    for (let i = 0; i < this.sounds.length; i++) {
      const element = this.sounds[i];
      element.volume(volume);
    }
  }

  play(drumType: DrumTypes) {
    if (drumType === DrumTypes.Crash) {
      this.sounds[0].play();
    }
    if (drumType === DrumTypes.Snare) {
      this.sounds[1].play();
    }
    if (drumType === DrumTypes.Kick) {
      this.sounds[2].play();
    }
    if (drumType === DrumTypes.HiHatClosed) {
      this.sounds[3].play();
    }
    if (drumType === DrumTypes.HiHatOpen) {
      this.sounds[4].play();
    }
    if (drumType === DrumTypes.Ride) {
      this.sounds[5].play();
    }
    if (drumType === DrumTypes.HighTom) {
      this.sounds[6].play();
    }
    if (drumType === DrumTypes.MidTom) {
      this.sounds[7].play();
    }
    if (drumType === DrumTypes.FloorTom) {
      this.sounds[8].play();
    }
  }
}
