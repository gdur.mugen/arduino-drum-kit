
import { defineComponent, ref, PropOptions } from "@vue/composition-api";

export function makeLocalStorageable(localStorageKey: string, defaultValue: any) {
    const refValue = ref(defaultValue);
  
    if (localStorage.hasOwnProperty(localStorageKey)) {
      refValue.value = parseInt(localStorage.getItem(localStorageKey) || "0");
    }
  
    const onInput = (event: InputEvent) => {
      localStorage.setItem(localStorageKey, refValue.value.toString());
    };
    return { refValue, onInput };
  }